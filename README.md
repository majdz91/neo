# README #


to run the app make sure you have docker & docker-compose pre-installed
open terminal and cd project dir 
run docker-compose up 

once the process is done you can confirm installation by visiting http//:0.0.0.0:8080/api/pdf
to test the app with pdf file install postman 
and then create post request to http://0.0.0.0:8080/api/pdf
set header Content-Type to application/x-www-form-urlencoded
go to request body and add pdf key set it to the pdf file you want to scan and 
submit request.
