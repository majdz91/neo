<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExportDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_document', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('total', 9, 2);
            $table->timestamp('created_at')->nullable();
            $table->float('weight', 9, 2);
            $table->string('weight_unit', 2);
            $table->string('airway_bill_number')->unique();
            $table->string('reference_number');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_document');
    }
}
