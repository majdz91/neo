<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Symfony\Component\Process\Process;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PdfController extends Controller
{
    public function upload(Request $request)
    {
        $request->validate([
            'pdf' => 'required|mimes:pdf|max:2048',
        ]);
        $file = $request->files->get('pdf');
        $data = $request->all();
        try {
            $file->move(storage_path(), 'input.pdf');
            $process =  Process::fromShellCommandline('cd /var/www/html/app/; nodejs app.js');
            $process->run();
            $data = json_decode(file_get_contents(storage_path().'/output.json'));
            
            return new JsonResponse($data, 200);
        } catch(\Throwable $ex) {
            return new JsonResponse(['error' => $ex->getMessage()]);
        }
    }
}