module.exports = {
    totalValue: {
      aliases: ["end value", "total value", "totalvalue"],
      valueFormat: "decimal"
    },
    weight: {
      aliases: ["weight"],
      valueFormat: "decimal"
    },
    weightUnit: {
      aliases: ["weight unit", "weightunit", "unit"],
      valueFormat: "weight"
    },
    incoterm: {
      aliases: [],
      valueFormat: "incoterm"
    },
    airwayBill: {
      aliases: [
        "airwaybill",
        "airway bill",
        "airway bill no",
        "airway bill number"
      ],
      valueFormat: "number"
    }
  }
  